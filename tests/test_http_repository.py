###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from unittest.mock import patch

import pytest

from lb.nightly.utils.Repository import HttpRepository, connect


@pytest.mark.parametrize(
    "config,uri,expected",
    [
        (
            {"artifacts": {"user": "someuser", "password": "Sup3rS3cre7"}},
            "https://hostname:1234/some/path",
            ("https://hostname:1234/some/path", "someuser", "Sup3rS3cre7"),
        ),
        (
            {
                "artifacts": {
                    "uri": "https://hostname:1234/some/path",
                    "user": "someuser",
                    "password": "Sup3rS3cre7",
                }
            },
            None,
            ("https://hostname:1234/some/path", "someuser", "Sup3rS3cre7"),
        ),
        (
            {"artifacts": {"password": "Sup3rS3cre7"}},
            "https://someuser@hostname:1234/some/path",
            ("https://hostname:1234/some/path", "someuser", "Sup3rS3cre7"),
        ),
        (
            {},
            "https://someuser:Sup3rS3cre7@hostname:1234/some/path",
            ("https://hostname:1234/some/path", "someuser", "Sup3rS3cre7"),
        ),
        (
            {"artifacts": {"user": "someuser", "password": "Sup3rS3cre7"}},
            "https://nobody:4321@hostname:1234/some/path",
            ("https://hostname:1234/some/path", "someuser", "Sup3rS3cre7"),
        ),
    ],
)
def test_connect(config, uri, expected):
    with patch("lb.nightly.configuration.service_config", return_value=config):
        r = connect(uri)
        assert isinstance(r, HttpRepository)
        uri, user, password = expected
        assert r.uri == uri
        assert r.auth == (user, password)
        assert r.get_url("my_artifact") == "https://hostname:1234/some/path/my_artifact"


def test_connect_missing_credentials():
    with patch("lb.nightly.configuration.service_config", return_value={}):
        with pytest.raises(KeyError):
            connect("https://someuser@hostname:1234/some/path")
        with pytest.raises(KeyError):
            connect("https://hostname:1234/some/path", password="stuff")
