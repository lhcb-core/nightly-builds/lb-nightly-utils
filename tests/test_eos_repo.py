###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from os.path import exists
from shutil import copyfileobj
from subprocess import CalledProcessError, CompletedProcess
from tempfile import mkstemp
from unittest.mock import ANY, call, mock_open, patch

from lb.nightly.utils.Repository import EosRepository, connect

_testdata = os.path.normpath(
    os.path.join(*([__file__] + [os.pardir] * 1 + ["testdata"]))
)


patch_conf_eos = patch(
    "lb.nightly.configuration",
    **{
        "service_config.return_value": {
            "artifacts": {
                "uri": "root://eoslhcb.cern.ch//eos/some/path",
                "keytab": "/path/to/keytab",
                "user": "lhcbsoft@CERN.CH",
            }
        }
    },
)

patch_run_ok = patch(
    "lb.nightly.utils.Repository.run",
    **{"return_value.returncode": 0},
)


@patch_run_ok
@patch_conf_eos
def test_connect_repo(ar, mr):
    assert isinstance(connect(), EosRepository)


@patch_run_ok
@patch(
    "lb.nightly.configuration",
    **{
        "service_config.return_value": {
            "artifacts": {
                "uri": "root://eoslhcb.cern.ch//eos/some/path",
                "password": "pass",
                "user": "lhcbsoft@CERN.CH",
            }
        }
    },
)
def test_connect_repo_pass(ar, pm):
    assert isinstance(connect(), EosRepository)


@patch_run_ok
@patch_conf_eos
def test_eos_push(conf, run):
    repo = connect()
    src = os.path.join(
        _testdata, "artifacts", "packs", "src", "TestProject.HEAD.testing-slot.src.zip"
    )
    dst = "TestProjectPush.HEAD.testing-slot.src.zip"
    with open(src, "rb") as f:
        assert repo.push(f, dst) == True
    print(run.mock_calls)
    with repo._authenticated_env() as env:
        run.assert_has_calls(
            [
                call(
                    [
                        "kinit",
                        "-c",
                        repo._krb_token.name,
                        "-k",
                        "-t",
                        "/path/to/keytab",
                        "lhcbsoft@CERN.CH",
                    ],
                    check=True,
                ),
                call(
                    [
                        "xrdfs",
                        "root://eoslhcb.cern.ch",
                        "mkdir",
                        "-p",
                        "/eos/some/path/",
                    ],
                    check=True,
                    env=env,
                ),
                call(
                    [
                        "xrdcp",
                        "-f",
                        ANY,
                        "root://eoslhcb.cern.ch//eos/some/path/TestProjectPush.HEAD.testing-slot.src.zip",
                    ],
                    check=True,
                    env=env,
                ),
            ]
        )


@patch(
    "lb.nightly.utils.Repository.run",
)
@patch_conf_eos
def test_eos_push_fail(conf, run):
    run.side_effect = [
        CompletedProcess(args=[], returncode=0),
        None,
        CalledProcessError(cmd=[], returncode=22),
    ]
    repo = connect()
    src = os.path.join(
        _testdata, "artifacts", "packs", "src", "TestProject.HEAD.testing-slot.src.zip"
    )
    dst = "TestProjectPush.HEAD.testing-slot.src.zip"
    with open(src, "rb") as f:
        assert repo.push(f, dst) == False


@patch_run_ok
@patch_conf_eos
def test_eos_exist(conf, run):
    repo = connect()
    assert repo.exist("artifact.zip") == True


@patch(
    "lb.nightly.utils.Repository.run",
)
@patch_conf_eos
def test_eos_not_exist(conf, run):
    run.side_effect = [
        CompletedProcess(args=[], returncode=0),
        CalledProcessError(returncode=22, cmd=[]),
    ]
    repo = connect()
    assert repo.exist("artifact.zip") == False


@patch("lb.nightly.utils.Repository.open", mock_open(read_data=b"1"))
@patch_run_ok
@patch_conf_eos
def test_eos_pull(conf, run):
    artifact = "artifact.zip"
    fd, path = mkstemp()
    repo = connect()
    with open(path, "wb") as lp:
        with repo.pull(artifact) as remote:
            copyfileobj(remote, lp)
            assert remote.getvalue() == b"1"

    assert exists(path)
    with open(path, "rb") as lp:
        assert lp.read() == b"1"

    os.close(fd)
    os.remove(path)
