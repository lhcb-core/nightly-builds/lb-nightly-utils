###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

import pytest

from lb.nightly.utils.Repository import FileRepository


@pytest.mark.parametrize(
    "param",
    [
        {"uri": "file:///tmp/some/dir", "root": "/tmp/some/dir"},
        {"uri": "/tmp/some/dir", "root": "/tmp/some/dir"},
        {"uri": "some/dir", "root": os.path.join(os.getcwd(), "some/dir")},
        {"uri": "file:some/dir", "root": os.path.join(os.getcwd(), "some/dir")},
    ],
)
def test_constructor(param):
    r = FileRepository(param["uri"])
    assert r.uri == param["uri"]
    assert r.root == param["root"]
