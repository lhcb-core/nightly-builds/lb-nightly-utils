import os
from shutil import copyfileobj
from tempfile import NamedTemporaryFile
from unittest.mock import patch

import boto3
import pytest
from moto import mock_s3

from lb.nightly.utils.Repository import S3Repository, connect


@pytest.fixture(scope="function")
def aws_credentials():
    """Mocked AWS Credentials for moto."""
    os.environ["AWS_ACCESS_KEY_ID"] = "testing"
    os.environ["AWS_SECRET_ACCESS_KEY"] = "testing"
    os.environ["AWS_SECURITY_TOKEN"] = "testing"
    os.environ["AWS_SESSION_TOKEN"] = "testing"
    os.environ["AWS_DEFAULT_REGION"] = "us-east-1"
    os.environ["MOTO_S3_CUSTOM_ENDPOINTS"] = "https://my-s3.cern.ch"


@pytest.fixture(scope="function")
def s3_client(aws_credentials):
    with mock_s3():
        yield boto3.client(
            "s3", endpoint_url="https://my-s3.cern.ch", region_name="us-east-1"
        )


@pytest.fixture
def bucket_name():
    return "nightlies"


@pytest.fixture
def s3_test(s3_client, bucket_name):
    s3_client.create_bucket(Bucket=bucket_name)
    yield


patch_conf_eos = patch(
    "lb.nightly.configuration",
    **{
        "service_config.return_value": {
            "artifacts": {
                "uri": "s3+https://my-s3.cern.ch",
                "region_name": None,
                "access_key_id": None,
                "secret_access_key": None,
                "bucket": "nightlies",
            }
        }
    },
)


@patch_conf_eos
def test_push_exist_pull(s3_client, s3_test):
    my_client = connect()
    artifact_content = b"That's the content of the artifact"
    with NamedTemporaryFile() as tmp:
        tmp.write(artifact_content)
        tmp.seek(0)
        artifact = os.path.basename(tmp.name)
        assert not my_client.exist(artifact)
        assert my_client.push(tmp, artifact)
        assert my_client.exist(artifact)

    with NamedTemporaryFile() as tmp:
        with my_client.pull(artifact) as remote:
            copyfileobj(remote, tmp)
        tmp.seek(0)
        assert tmp.read() == artifact_content


@patch_conf_eos
def test_repo_connect(conf, s3_client, s3_test, aws_credentials):
    repo = connect()
    assert repo.uri == "https://my-s3.cern.ch"
    assert repo.bucket == "nightlies"


@patch_conf_eos
def test_repo_get_url(conf, s3_client, s3_test, aws_credentials):
    repo = connect()
    assert "https://my-s3.cern.ch/nightlies/my_artifacts" == repo.get_url(
        "my_artifacts"
    )
