###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
from filecmp import cmp
from os.path import exists, join, normpath
from shutil import copyfile, copyfileobj
from tempfile import mkstemp
from unittest.mock import patch

from lb.nightly.utils.Repository import connect

_testdata = normpath(join(*([__file__] + [os.pardir] * 1 + ["testdata"])))
artifacts_repo = join(_testdata, "artifacts_repo")

patch_conf_eos = patch(
    "lb.nightly.configuration",
    **{
        "service_config.return_value": {
            "artifacts": {
                "uri": artifacts_repo,
            }
        }
    },
)


@patch_conf_eos
def test_repo_push(conf):
    src = join(
        _testdata, "artifacts", "packs", "src", "TestProject.HEAD.testing-slot.src.zip"
    )
    dst = "TestProjectPush.HEAD.testing-slot.src.zip"
    repo = connect()
    with open(src, "rb") as f:
        repo.push(f, dst)

    assert exists(join(artifacts_repo, dst))
    assert cmp(src, join(artifacts_repo, dst))
    os.remove(join(artifacts_repo, dst))


@patch_conf_eos
def test_repo_pull(conf):
    artifact = "TestProject.HEAD.testing-slot.src.zip"
    src = join(
        _testdata, "artifacts", "packs", "src", "TestProject.HEAD.testing-slot.src.zip"
    )
    dst = join(artifacts_repo, artifact)
    copyfile(src, dst)
    fd, path = mkstemp()
    repo = connect()
    with open(path, "wb") as lp:
        with repo.pull(artifact) as remote:
            copyfileobj(remote, lp)

    assert exists(path)
    assert cmp(join(artifacts_repo, artifact), path)
    os.close(fd)
    os.remove(path)
    os.remove(dst)
