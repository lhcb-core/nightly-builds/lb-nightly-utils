###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import json
import os
from subprocess import run

from lb.nightly.configuration import DBASE, DataProject, Package, Project, Slot

from lb.nightly.utils import make_slot_deployment_dir


def test_file_output(tmp_path):
    slot = Slot(
        name="test_slot",
        projects=[
            Project("LCG", "101", disabled=True),
            DBASE(packages=[Package("PRConfig", "HEAD")]),
            Project("Gaudi", "v36r2"),
            Project("LHCb", "HEAD"),
        ],
        platforms=["x86_64_v2-centos7-gcc11-opt", "x86_64_v2-centos7-gcc11-dbg"],
    )

    test_zip = tmp_path / "artifact.zip"

    with test_zip.open("wb") as f:
        ret = make_slot_deployment_dir(slot, f)
        assert ret is f

    dest = tmp_path / "dest"
    dest.mkdir()

    run(["unzip", "-d", dest, test_zip])

    assert (dest / "slot-config.json").exists()
    assert json.load((dest / "slot-config.json").open()) == slot.toDict()
    for project in slot.projects:
        for platform in slot.platforms:
            if project.enabled:
                if isinstance(project, DataProject):
                    assert (dest / project.name).is_dir()
                    assert not (dest / project.name / "InstallArea" / platform).exists()
                    for package in project.packages:
                        assert (dest / project.name / package.name).is_dir()
                else:
                    assert (dest / project.name / "InstallArea" / platform).is_dir()
            else:
                assert not (dest / project.name).exists()


def test_memory_output(tmp_path):
    slot = Slot(
        name="test_slot",
        projects=[Project("Gaudi", "v36r2"), Project("LHCb", "HEAD")],
        platforms=["x86_64_v2-centos7-gcc11-opt", "x86_64_v2-centos7-gcc11-dbg"],
    )

    test_zip = tmp_path / "artifact.zip"

    ret = make_slot_deployment_dir(slot)

    test_zip.write_bytes(ret.read())

    dest = tmp_path / "dest"
    dest.mkdir()

    run(["unzip", "-d", dest, test_zip])

    assert (dest / "slot-config.json").exists()
    assert json.load((dest / "slot-config.json").open()) == slot.toDict()
    for project in slot.projects:
        for platform in slot.platforms:
            assert (dest / project.name / "InstallArea" / platform).is_dir()
